// 'cma comms.js'
// Copyright 2019 Connected Media Australia Pty Ltd.
// 20190904 v1.0 Rod Driscoll

// dependencies:
//  'cma log.js'
//      - log.Write(log.Level, data)
//  'ConfigSettings.xml'
//      - "ConnectionType"
//          - "SerialPort","BaudRate","DataBits","StopBits","Parity","Handshaking","DataBits"
//          - "TCPAddress", TCPPort
//  'SystemVariables.xml'
//      - "ConnectionState", "ConnectionState00", "ConnectionState01"
//      - "InitializeState", "InitializeState00", "InitializeState01", "InitializeState02"
//  'SystemEvents.xml'
//      - "CONNECTIONSTATE00", "CONNECTIONSTATE01"
//      - "INITIALESTATE00", "INITIALESTATE01"
//  'Kramer Protocol 3000.js'
//      - OnCommRx(data)
//      - g_RxStartChar, g_RxStopChar
//      - g_Init_Timer

var g_comm;
var g_ConnectionType = parseInt((Config.Get("ConnectionType")),10);

///////////////////////////////
// Initialize the comm ports
///////////////////////////////
log.Write(log.Message, 'Connection Type ' + g_ConnectionType);

switch (g_ConnectionType) {
	case 0:
		log.Write(log.Level.Message, 'Using serial port '+parseInt(Config.Get("SerialPort"))
																+','+parseInt(Config.Get("BaudRate"))
																+','+parseInt(Config.Get("DataBits"))
																+','+parseInt(Config.Get("StopBits"))
																+','+Config.Get("Parity").toString());
		g_comm = new Serial(OnCommRx, parseInt(Config.Get("SerialPort"))
									, parseInt(Config.Get("BaudRate"))
									, parseInt(Config.Get("DataBits"))
									, parseInt(Config.Get("StopBits"))
									, Config.Get("Parity").toString()
									, Config.Get("Handshaking").toString());
		break;
	case 1:
		log.Write(log.Level.Message, 'Using IP address '+Config.Get("TCPAddress").toString()+':'+parseInt(Config.Get("TCPPort")));
		g_comm = new TCP(OnCommRx, Config.Get("TCPAddress").toString(), parseInt(Config.Get("TCPPort")));
		break;
	default:
		log.Write(log.Level.Message, 'Connection type ' + g_ConnectionType + ' out of range');
		break;
}


if(g_SRx && g_ERx)
	g_comm.AddRxFraming("StartStopChar", g_SRx, g_SEx);
else if(g_SRx)
	g_comm.AddRxFraming("StartChar", g_SRx);
else if(g_SEx)
	g_comm.AddRxFraming("StopChar", g_SEx);
g_comm.EnableHeartbeat(8000, OnSendHeartbeat, OnConnect, OnDisconnect);


function OnConnect() {
	g_Init_Timer.Stop();
	log.Write(log.Level.Message, 'Connected');
	ConnectionStateChange(1);
	g_Init_Timer.Start(StartInit,500);
}

function OnDisconnect() {
	log.Write(log.Level.Message, 'Disconnected');
	InitializeStateChange(0);
	ConnectionStateChange(0);
}


///////////////////////////////
//  Variable Change Functions
///////////////////////////////
function ConnectionStateChange(newState) {
	if (newState!=SystemVars.Read("ConnectionState")) {
		switch(newState) {
			case 0:
				log.Write(log.Level.Notice, 'Disconnected');
				System.SignalEvent("CONNECTIONSTATE00");
				break;
			case 1:
				log.Write(log.Level.Notice, 'Connected');
				System.SignalEvent("CONNECTIONSTATE01");
				break;
		}
		SystemVars.Write("ConnectionState",parseInt(newState,10));
		SystemVars.Write("ConnectionState00",parseInt(newState,10)==0);
		SystemVars.Write("ConnectionState01",parseInt(newState,10)==1);
	}
}

function InitializeStateChange(newState) {
	if (newState!=SystemVars.Read("InitializeState"))  {
		switch(newState) {
			case 0:
				log.Write(log.Level.Notice, 'Not Initialized');
				System.SignalEvent("INITIALESTATE00");
				g_Init_Pass = 1;
				break;
			case 1:
				log.Write(log.Level.Notice, 'Initializing');
				System.SignalEvent("INITIALESTATE01");
				break;
			case 2:
				log.Write(log.Level.Notice, 'Initialized');
				break;
		}
		SystemVars.Write("InitializeState"  ,parseInt(newState,10));
		SystemVars.Write("InitializeState00",parseInt(newState,10)==0);
		SystemVars.Write("InitializeState01",parseInt(newState,10)==1);
		SystemVars.Write("InitializeState02",parseInt(newState,10)==2);
	}
}