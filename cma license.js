// 'cma license.js'
// Copyright 2019 Connected Media Australia Pty Ltd.
// 20190912 v1.0 Rod Driscol

// dependencies:
//  'ConfigSettings.xml'
//      - "LicenseKey"
//  'cma log.js'
//      - log.Write(log.Type, data)

var license = (function () {
    'use strict';

    var key = Config.Get("LicenseKey");
    var mac = System.MACAddress;
    var startTime = System.GetLocalTime();
    
    var runningTimer = new Timer();

/* Time Variables
    var local_time_in_seconds = System.GetLocalTimeInSeconds();
    var utc_time_in_seconds   = System.GetUTCTimeInSeconds();
    var timeDif;
    if (local_time_in_seconds > utc_time_in_seconds) {
        timeDif = ((local_time_in_seconds-utc_time_in_seconds)*1);							
    }
    else {
        timeDif = ((utc_time_in_seconds-local_time_in_seconds)*(-1));						
    }
    
    log.Write(log.Type.Always, 'License, time shift: '+(timeDif/360)+'\r\n');
*/
    var runTime = System.GetLocalTime() - startTime;
    log.Write(log.Type.Always, 'License: system has been running for: ' + runTime);

	runningTimer.Start(StartInit, 1000);
    //runningTimer.Stop();
    
        
    function StartInit() {
        runningTimer.Stop();
        var runTime = System.GetLocalTime() - startTime;
        log.Write(log.Type.Always, 'License: system has been running for: ' + runTime.getMinutes());
        runningTimer.Start(InitZones,500);
    }

    return {
        Key: key,
        RunningTimer: runningTimer
    }

})();

log.Write(log.Type.Always, 'cma license.js started');