// 'cma log.js'
// Copyright 2019 Connected Media Australia Pty Ltd.
// 20190904 v1.0 Rod Driscol

// dependencies:
//  'ConfigSettings.xml'
//      - "DebugTrace"
//      - "DebuggerName"

var g_driverName = Config.Get("DebuggerName");

var log = (function () {
    'use strict';

    var debugLevel = Config.Get("DebugTrace")

    var level = {
        Off    : 0,
        Low    : 1,
        Medium : 2,
        High   : 3
    }

    var type = {
        None   : 0,
        Info   : 1,
        Message: 2,
        Notice : 3,
        Error  : 4,
        Always : 5
    }

    var write = function (priority, data) {
        /*
        console.log(data);
        */
        switch(priority) {
            case type.Error  : // always
                System.LogError(data); 
                System.Print(g_driverName + ': ' + data);
                break;
            case type.Notice : 
                if (System.LogLevel >= level.Low)
                    System.LogInfo(level.Low, data); 
                if(debugLevel >= level.Low)
                    System.Print(g_driverName + ': ' + data);
                break;
            case type.Message:
                if (System.LogLevel >= level.Medium)
                    System.LogInfo(level.Medium, data); 
                if(debugLevel >= level.Medium)
                    System.Print(g_driverName + ': ' + data);
                break;
            case type.Info:
                if (System.LogLevel >= level.High)
                    System.LogInfo(level.High, data); 
                if(debugLevel >= level.High)
                    System.Print(g_driverName + ': ' + data);
                break;
            case type.Always:
                System.LogInfo(level.High, data); 
                System.Print(g_driverName + ': ' + data);
                break;
            default:
                write(type.Info, data);
                break;
        }
    }
    
    return {
        Write: write,
        Level: level,
        Type: type,
        DebugLevel: debugLevel
    }

})();

log.Write(log.Type.Always, 'cma log.js started, logLEvel: '+ System.LogLevel);