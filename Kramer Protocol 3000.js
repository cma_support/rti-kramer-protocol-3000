// Copyright 2019 Connected Media Australia.
// 20190604 v1.0 Rod Driscoll

// dependencies:
//  'cma log.js'
//      - log.Write(log.Type, data)
//  'cma strings.js'
//      - padDigits(val, totalDigits)

if (g_driverName == "") {
	System.LogError('driverName not defined'); s
	g_driverName = 'Kramer Protocol 3000';
	System.Print('ERROR: driverName not defined');
}

if (typeof log !== 'object') {
	System.LogError('log not defined'); 
	System.Print('ERROR: log not defined');
	log = {};
}
else 
	log.Write(log.Type.Always, ' loading');

//  'cma comms.js'
//      - g_comm
//      - g_ConnectionType

var g_SRx = '~';
var g_ERx = '\n';
var g_STx = '#';
var g_ETx = '\r';

// 
var g_Init_Timer = new Timer();
var g_Init_Pass = 1;

var g_OutputCount = parseInt((Config.Get("OutputCount")),10);
var g_InputCount = parseInt((Config.Get("InputCount")),10);

//log.Write(log.Level.Info, 'Initializing ');

function OnSendHeartbeat() {
	log.Write(log.Level.Info, 'Send Heartbeat');
	send(''); // '"#\r", Protocol handshaking, returns '~nn@ OK\r\n'
}

///////////////////////////////
//  OnCommRx
///////////////////////////////
function OnCommRx(data) {
	log.Write(log.Level.Message, 'Rx: ' + data);
	parseRx(data);
}

var ParseSerialNumber = function(data) {
	log.Write(log.Level.Info, 'Serial Number: ' + data);
};
var ParseModel = function(data) {
	log.Write(log.Level.Info, 'Model: ' + data);
};
var ParseFactoryReset = function(data) {
	log.Write(log.Level.Error, 'Factory reset: ' + data);
};
var ParseSystemReset = function(data) {
	log.Write(log.Level.Notice, 'System reset: ' + data);
};
var ParseProtocolVersion = function(data) {
	log.Write(log.Level.Info, 'Protocol version: ' + data); // 3000:XX.XX
};
var ParseFirmwareVersion = function(data) {
	log.Write(log.Level.Info, 'Firmware version: ' + data); // XX.XX.XXXX
};
var ParseBuildDate = function(data) { // ParseBuildDate('2019/08/30 12:20:00');
	var data = '2019/08/30 12:20:00';
	//var parse = /(?<year>\d{4})\/(?<month>\d{2})\/(?<day>\d{2}) (?<hour>\d{2}):(?<minute>\d{2}):(?<second>\d{2})/.exec(data);
	var buildDate = new Date(data);
	var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' };
	log.Write(log.Level.Info, 'Build date: ' + buildDate.toLocaleDateString("en-US", options));
};
var ParseIOInfo = function(data) {
	var arr = /IN (\d+),OUT (\d+)/.exec(data);
	log.Write(log.Level.Info, 'INFO-IO, inputs: ' + arr[1]
						+ ' outputs: ' + arr[2]); 
};
var ParseRoute = function(data) {
	var arr = /(\d+),(\d+),(\d+)/.exec(data);
	var layer  = arr[1];
	var output = arr[2];
	var input  = arr[3];
	log.Write(log.Level.Message, 'ROUTE, layer: ' + getKeyByValue(routeLayer, layer)
							+ ', output: ' + output
							+ ', input: '  + input); 
	switch(layer) {
		case routeLayer.VIDEO : 
			System.SignalEvent("Z"+output+"VIDEOSOURCE");
			SystemVars.Write("SrcForVideoOutput"+output, input);
			for(var i = 1; i <= g_InputCount; i++) {
				SystemVars.Write("VideoOutput"+Output+"S"+i,parseInt(+input,10)==i);
			}
			break;
		case routeLayer.AUDIO : 
			System.SignalEvent("Z"+output+"AUDIOSOURCE");
			SystemVars.Write("SrcForAudioOutput"+output, input);
			for(var i = 1; i <= g_InputCount; i++) {
				SystemVars.Write("AudioOutput"+Output+"S"+i,parseInt(+input,10)==i);
			}
			break;
	}
};

var RxFunctionDictinonary = {
	'SN'        : ParseSerialNumber,
	'INFO-IO?'  : ParseIOInfo,
	'MODEL'     : ParseModel,
	'BUILD-DATE': ParseBuildDate,
	'VERSION'   : ParseFirmwareVersion,
	'PROT-VER'  : ParseProtocolVersion,
	'FACTORY'   : ParseFactoryReset,
	'RESET'     : ParseSystemReset,
	'ROUTE'     : ParseRoute
}

function parseRx(data) {							//'~00@INFO-IO? IN inputs_count,OUT outputs_count\r\n'
	var parse = /^~(\d{2})@(.+)\r\n/.exec(x);
	if(parse == null) {
		log.Write(log.Level.Notice, 'no handler for: ' + data);
	} else {
		var device_id        = parse[1];				// '00'
		var command_data_all = parse[2];				// 'INFO-IO? IN inputs_count,OUT outputs_count'	
		var parse2 = /([^ ]*) (.*)/.exec(command_data_all);
		if(parse2 == null) {
			log.Write(log.Level.Notice, 'no handler for data: ' + command_data_all);
		} else {
			var command_in		= parse2[1];					// 'INFO-IO?'
			var command_data    = parse2[2];					// 'IN inputs_count,OUT outputs_count'	
			log.Write(log.Level.Info, 'command_in  : ' + command_in);
			log.Write(log.Level.Info, 'Command_data: ' + command_data);
			if(command_data === 'OK' && command_in.length < 1) {
				log.Write(log.Level.Info, 'machine number: ' + device_id);
			} else {
				try {
					var result = RxFunctionDictinonary[command_in](command_data);
				} catch (error) {
					log.Write(log.Level.Notice, 'no handler for: ' + command_in + '(' + command_data + ')');
				}
			}
		}
	}
}

function testRx() {
	parseRx('~00@SN 1234567890\r\n');
	parseRx('~00@INFO-IO? IN 4,OUT 1\r\n');
	parseRx('~00@ OK\r\n');
	parseRx('~00@MODEL model_name\r\n');
	parseRx('~00@BUILD-DATE 2019/08/30 12:20:00\r\n');
	parseRx('~00@FACTORY OK\r\n');
	parseRx('~00@ROUTE 1,2,3\r\n');
}

///////////////////////////////
//  Driver Executed Functions
///////////////////////////////

var routeLayer = { // getKeyByValue(routeLayer,"1");
	VIDEO: '1',
	AUDIO: '2',
	DATA : '3',
	IR   : '4',
	USB  : '5'
}
//log.Write(log.Level.Info, Object.keys(routeLayer));

function getKeyByValue(object, value) { //getKeyByValue(routeLayer, '1')
	//return Object.keys(object).find(key => object[key] === value);
	var keys = Object.keys(object);
	//log.Write(log.Level.Info, 'keys length: ' + keys.length);
	keys.forEach(function(element) { // keys.find isn't supported
		//log.Write(log.Level.Info, object[element]);
		if(object[element] == value)
			log.Write(log.Level.Info, 'key: ' + element + ', value: ' + object[element]);
	});
}

//getKeyByValue(routeLayer, '2');
///////////////////////////////
//  General Functions
///////////////////////////////
function RouteVideo(input, output) {//'#ROUTE? <layer>,<dest>\r'; // 1:video, 2:audio, 3:data, 4:ir, 5:usb
	log.Write(log.Level.Message, 'RouteLayer: '+ routeLayer);
	log.Write(log.Level.Message, 'RouteLayer.VIDEO: '+ routeLayer.VIDEO);
	log.Write(log.Level.Message, 'RouteVideo('+ routeLayer.VIDEO + ',' + input + ',' + output + ')');
	var str = 'ROUTE? '+ routeLayer.VIDEO + ',' + output + ', '+ input;
	send(str); // Protocol handshaking, returns '~nn@ OK\r\n'
}

function send(str) {
	log.Write(log.Level.Notice, 'Tx: ' + g_STx + str);
	g_comm.Write(g_STx + str + g_ETx);
}