// 'cma stringss.js'
// Copyright 2019 Connected Media Australia Pty Ltd.
// 20190912 v1.0 Rod Driscoll

// dependencies:
//

var strings = (function () {
    'use strict';

    var padDigits = function (n, totalDigits) {
        var i, pd;
        n = n.toString();
        pd = "";
        if (totalDigits > n.length) {
            for (i = 0; i < (totalDigits - n.length); i++)  {
                pd += "0";
            }
        }
        return pd + n;
    }

})();

log.Write(log.Level.Always, 'cma strings.js loaded');